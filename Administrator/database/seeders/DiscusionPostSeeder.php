<?php

namespace Database\Seeders;

use App\Models\DiscusionPost;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DiscusionPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new DiscusionPost();
        $post->title = "PHP CLI PROBLEM"; 
        $post->desc = "Php dont work correctly bla bla bla";
        $post->topic_id = 1;
        $post->user_id = 1;
        $post->save();
    }
}
