<?php

namespace Database\Seeders;

use App\Models\Topics;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['php', 'js', 'laravel'];

        for ($i=0; $i < sizeof($data) ; $i++) { 
            $post = new Topics();
            $post->title = $data[$i]; 
            $post->save();
        }
    }
}
