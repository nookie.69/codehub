<?php

namespace Database\Seeders;

use App\Models\DiscusionComment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DiscusionCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new DiscusionComment();
        $post->comment = "Coment by user 2"; 
        $post->discusion_id = 1;
        $post->user_id = 2;
        $post->save();
    }
}
