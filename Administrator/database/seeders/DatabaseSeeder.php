<?php
namespace Database\Seeders;

use App\Models\Discusion;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(20)->create();
        $this->call([
            UsersTableSeeder::class,
            PostsTableSeeder::class,
            EventTypesSeeder::class,
            EventsSeeder::class,
            ImagesSeeder::class,
            TopicSeeder::class,
            DiscusionPostSeeder::class,
            DiscusionCommentSeeder::class
        ]);
    }
}
