<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscusionComment extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function discusion() {
        return $this->belongsTo(DiscusionPost::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
