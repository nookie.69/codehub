<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscusionPost extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function topic() {
        return $this->belongsTo(Topics::class);
    }

    public function comments() {
        return $this->hasMany(DiscusionComment::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
