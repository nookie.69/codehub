@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'title' => __('Administrator')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
          {{-- {{ dd($topics) }} --}}
          @foreach ($topics as $topic)
            <a href="{{ route('discussion', ['id' => $topic->id]) }}" class="text-danger h3 "><p class="bg-white w-100 text-center">{{ $topic->title }}</p></a>
          @endforeach
      </div>
  </div>
</div>
@endsection
