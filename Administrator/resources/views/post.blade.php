@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'title' => __('Administrator')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
        <p>{{ $discussion[0]->title }}</p>
        <p>{{ $discussion[0]->desc }}</p>

        @foreach ($posts as $post)
        <p class="w-100 text-center">{{ $post->comment }}</p>
        <p class=" w-100 text-center">{{ $post->user->name }}</p>
        @endforeach
      </div>
  </div>
</div>
@endsection
